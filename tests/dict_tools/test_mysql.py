from dict_tools import mysql


def test_to_dict():
    test_mysql_output = [
        "+----+------+-----------+------+---------+------+-------+------------------+",
        "| Id | User | Host      | db   | Command | Time | State | Info             |",
        "+----+------+-----------+------+---------+------+-------+------------------+",
        "|  7 | root | localhost | NULL | Query   |    0 | init  | show processlist |",
        "+----+------+-----------+------+---------+------+-------+------------------+",
    ]

    ret = mysql.to_dict(test_mysql_output, "Info")
    expected_dict = {
        "show processlist": {
            "Info": "show processlist",
            "db": "NULL",
            "State": "init",
            "Host": "localhost",
            "Command": "Query",
            "User": "root",
            "Time": 0,
            "Id": 7,
        }
    }

    assert ret == expected_dict
